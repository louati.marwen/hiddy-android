package com.hitasoft.app.model;

import com.google.gson.annotations.SerializedName;

public class SaveMyContacts {
    @SerializedName("status")
    public String status;
    @SerializedName("message")
    public String message;
}
