package com.hitasoft.app.helper;

public interface OnLoadMoreListener {
    void onLoadMore();
}
