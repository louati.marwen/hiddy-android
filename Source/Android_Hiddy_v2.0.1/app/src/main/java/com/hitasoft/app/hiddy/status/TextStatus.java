package com.hitasoft.app.hiddy.status;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.helper.StorageManager;
import com.hitasoft.app.hiddy.ApplicationClass;
import com.hitasoft.app.hiddy.BaseActivity;
import com.hitasoft.app.hiddy.NewGroupActivity;
import com.hitasoft.app.hiddy.R;
import com.hitasoft.app.utils.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Random;

public class TextStatus extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "TextStatus";
    RelativeLayout parentLay, textLay, textLayout;
    ImageView changeColor, sendStatus;
    EditText editText;
    int textSize = 50, height = 0;
    double size = 1;
    StorageManager storageManager;
    InputFilter[] filters = new InputFilter[1];
    String beforeText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_status);
        parentLay = findViewById(R.id.parentLay);
        changeColor = findViewById(R.id.changeColor);
        sendStatus = findViewById(R.id.sendStatus);
        editText = findViewById(R.id.editText);
        textLay = findViewById(R.id.textLay);
        textLayout = findViewById(R.id.textLayout);
        storageManager = StorageManager.getInstance(this);

        height = editText.getLayoutParams().height;

        editText.setTextSize(textSize);

/*
        filters[0] = new InputFilter.LengthFilter(500);
        editText.setFilters(filters);*/

        changeBg();


        changeColor.setOnClickListener(this);
        sendStatus.setOnClickListener(this);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                beforeText = "" + s;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    sendStatus.setVisibility(View.VISIBLE);
                } else {
                    sendStatus.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    int l = editText.getLineCount();
                    if (l <= 16) {
                        if (l > 4 && l < 8) {
                            size = 1.8;
                        } else if (l >= 8 && l < 12) {
                            size = 2.1;
                        } else if (l >= 12) {
                            size = 2.4;
                        }
                        editText.setTextSize((float) (textSize / size));

                        if (s.length() > 500) {
                            editText.setText("" + beforeText);
                            editText.setSelection(beforeText.length() - 1);
                            ApplicationClass.showToast(TextStatus.this, getString(R.string.maximum_characters_limit_reached), Toast.LENGTH_SHORT);
                        }
                    } else {
                        editText.setText("" + beforeText);
                        editText.setSelection(beforeText.length() - 1);
                        ApplicationClass.showToast(TextStatus.this, getString(R.string.maximum_line_limit_reached), Toast.LENGTH_SHORT);
                    }

                }
                /*int l = editText.getLineCount();

                if(l<15 && s.length()<700) {
                    if (l < 10) {
                        size = 1.5;
                    }
                    editText.setTextSize((float) (textSize / size));
                } else {
                    int len= editText.getText().toString().length();
                    String temp = editText.getText().toString();
                    String str = temp.substring(0,len);
                    editText.setText(str);
                    editText.setSelection(len-1);
                    ApplicationClass.showToast(TextStatus.this,"limit Excced",Toast.LENGTH_SHORT);
                }*/

                /*editText.removeTextChangedListener(this);
                editText.setSelection(s.length());
                editText.addTextChangedListener(this);*/
            }
        });

    }


    @Override
    public void onNetworkChange(boolean isConnected) {
        ApplicationClass.showSnack(TextStatus.this, findViewById(R.id.parentLay), isConnected);
    }

    private void changeBg() {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        if (color == -1) {
            changeBg();
        } else {
            textLayout.setBackgroundColor(color);
        }
    }

    private void createImage() {
        /*textLayout.setDrawingCacheEnabled(true);
        textLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        textLayout.layout(0, 0, textLayout.getWidth(), textLayout.getHeight());

        textLayout.buildDrawingCache(true);*/
        editText.clearFocus();
        Bitmap thumb = viewToBitmap(textLayout);
        String fileName = getString(R.string.app_name) + "_" + System.currentTimeMillis() + ".JPG";
        File mDestDir = storageManager.getExtFilesDir();
        File mDestFile = new File(mDestDir.getPath() + File.separator + fileName);
        if (mDestDir != null) {
            try {
                FileOutputStream out = new FileOutputStream(mDestFile);
                thumb.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
                String imagePath = mDestFile.getAbsolutePath();
                HashMap<String, String> map = new HashMap<>();
                map.put(Constants.TAG_MESSAGE, "");
                map.put(Constants.TAG_ATTACHMENT, imagePath);
                map.put(Constants.TAG_TYPE, "image");
                Intent intent = new Intent(this, NewGroupActivity.class);
                intent.putExtra(Constants.TAG_FROM, "status");
                intent.putExtra(Constants.TAG_MESSAGE_DATA, map);
                startActivityForResult(intent, Constants.STATUS_TEXT_CODE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Bitmap viewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        editText.setCursorVisible(true);
        return bitmap;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.STATUS_TEXT_CODE) {
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.changeColor:
                changeBg();
                break;
            case R.id.sendStatus:
                ApplicationClass.preventMultiClick(sendStatus);
                editText.setCursorVisible(false);
                if (NetworkReceiver.isConnected()) {
                    createImage();
                } else {
                    ApplicationClass.showSnack(TextStatus.this, findViewById(R.id.parentLay), false);
                }
                break;
        }
    }
}
