package com.hitasoft.app.hiddy;

import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.hitasoft.app.helper.DatabaseHandler;
import com.hitasoft.app.helper.ForegroundService;
import com.hitasoft.app.helper.LocaleManager;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.helper.SocketConnection;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;


public abstract class BaseActivity extends AppCompatActivity implements NetworkReceiver.ConnectivityReceiverListener {

    private static final String TAG = "BaseActivity";
    NetworkReceiver networkReceiver;
    private boolean IS_NETWORK_CHANGED = false;
    DatabaseHandler dbhelper;
    SocketConnection socketConnection;

    private Thread.UncaughtExceptionHandler handleAppCrash =
            new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable e) {
                    final Writer result = new StringWriter();
                    final PrintWriter printWriter = new PrintWriter(result);
                    e.printStackTrace(printWriter);
                    printWriter.close();
                    StackTraceElement[] arr = e.getStackTrace();
                    StringBuilder report = new StringBuilder(e.toString() + "\n\n");
                    report.append("--------- Stack trace ---------\n\n");
                    for (int i = 0; i < arr.length; i++) {
                        report.append("    ").append(arr[i].toString()).append("\n");
                    }
                    report.append("-------------------------------\n\n");

                    report.append("--------- Cause ---------\n\n");
                    Throwable cause = e.getCause();
                    if (cause != null) {
                        report.append(cause.toString()).append("\n\n");
                        arr = cause.getStackTrace();
                        for (StackTraceElement stackTraceElement : arr) {
                            report.append("    ").append(stackTraceElement.toString()).append("\n");
                        }
                    }
                    report.append("-------------------------------\n\n");
                    sendEmail(report.toString());
                }
            };

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
        Log.d(TAG, "attachBaseContext");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }

        // register connection status listener
        ApplicationClass.getInstance().setConnectivityListener(this);
        Thread.setDefaultUncaughtExceptionHandler(handleAppCrash);


        dbhelper = DatabaseHandler.getInstance(this);
        socketConnection = SocketConnection.getInstance(this);

//        Timer timer = new Timer();
//        TimerTask timerTask = new TimerTask() {
//            @Override
//            public void run() {
//                // your code here...
//                if (socketConnection != null)
//                    socketConnection.runTimerTask("ping");
//            }
//        };
//        timer.schedule(timerTask, 0L, 10000);

        networkReceiver = new NetworkReceiver();
        registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }


    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (overrideConfiguration != null) {
            int uiMode = overrideConfiguration.uiMode;
            overrideConfiguration.setTo(getBaseContext().getResources().getConfiguration());
            overrideConfiguration.uiMode = uiMode;
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.v("onNetwork", "base=" + isConnected);
        if (isConnected && !ForegroundService.IS_SERVICE_RUNNING && IS_NETWORK_CHANGED) {
            IS_NETWORK_CHANGED = false;
            Log.v("onNetwork", "service start");
            socketConnection = SocketConnection.getInstance(this);
            Intent service = new Intent(this, ForegroundService.class);
            service.setAction("start");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(service);
            } else {
                startService(service);
            }
        } else {
            IS_NETWORK_CHANGED = true;
        }

        onNetworkChange(isConnected);
    }

    public abstract void onNetworkChange(boolean isConnected);

    @Override
    protected void onResume() {
        super.onResume();
        dbhelper = DatabaseHandler.getInstance(this);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (networkReceiver != null) {
            unregisterReceiver(networkReceiver);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void makeToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void showLog(String TAG, String message, String value) {
        Log.d(TAG, message + " :" + value);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void sendEmail(String crash) {
        try {

            String reportContetnt = "\n\n" + "DEVICE OS VERSION CODE: " + Build.VERSION.SDK_INT + "\n" +
                    "DEVICE VERSION CODE NAME: " + Build.VERSION.CODENAME + "\n" +
                    "DEVICE NAME: " + getDeviceName() + "\n" +
                    "VERSION CODE: " + BuildConfig.VERSION_CODE + "\n" +
                    "VERSION NAME: " + BuildConfig.VERSION_NAME + "\n" +
                    "PACKAGE NAME: " + BuildConfig.APPLICATION_ID + "\n" +
                    "BUILD TYPE: " + BuildConfig.BUILD_TYPE + "\n\n\n" +
                    crash;

            final Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:")); // only email apps should handle this
            emailIntent.putExtra(Intent.EXTRA_EMAIL,
                    new String[]{"crashlog@hitasoft.com"});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Crash Report");
            emailIntent.putExtra(Intent.EXTRA_TEXT, reportContetnt);
            try {
                //start email intent
                startActivity(Intent.createChooser(emailIntent, "Email"));
            } catch (Exception e) {
                //if any thing goes wrong for example no email client application or any exception
                //get and show exception message
                e.printStackTrace();
            }
        } catch (Exception e) {
            Log.e(TAG, "sendEmail: " + e.getMessage());
        }
    }

    /**
     * Returns the consumer friendly device name
     */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        return manufacturer + " " + model;
    }
}
