package com.hitasoft.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChannelChatResult {
    @SerializedName("status")
    public String status;
    @SerializedName("result")
    public List<ChannelMessage> result;
}
